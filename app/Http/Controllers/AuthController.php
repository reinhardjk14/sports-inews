<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use DB;

class AuthController extends Controller
{
    public function kirimRegister(Request $request){
        $request->validate([
            'username' => 'required|unique:users',
            'password' => 'required',
            'email' => 'required',
        ]);
        $query = DB::table('users')->insert([
            "username" => $request["username"],
            "password" => Hash::make($request["password"]),
            "email" => $request["email"],
            "role" => "Member",
            "foto" => "None"
        ]);
        return redirect('/');
    }
}
