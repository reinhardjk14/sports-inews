<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    public function dashboard(){
        $users = DB::table('users')->get();
        return view('admin.index', compact('users'));
    }

    public function profile(){
        $users = DB::table('users')->get();
        return view('admin.profile', compact('users'));
    }

    public function tambahberita(){
        return view('admin.tambahberita');
    }

    public function addberita(Request $request) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'tanggalTerbit' => 'required',
            'foto' => 'required',
        ]);
        $query = DB::table('berita')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggalTerbit" => $request["tanggalTerbit"],
            "foto" => $request["foto"]
        ]);
        return redirect('/dashboard');
    }

    public function register() {
        return view('admin.register');
    }

    public function login() {
        return view('admin.login');
    }
}
