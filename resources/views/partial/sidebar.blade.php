<ul class="menu">
    <li class='sidebar-title'>Main Menu</li>
    <li class="sidebar-item">
        <a href="/dashboard" class='sidebar-link'>
            <i data-feather="home" width="20"></i>
            <span>Dashboard</span>
        </a>
    </li>
    @if (Auth::user()->role === "Admin")
    <li class="sidebar-item  has-sub">
        <a href="#" class='sidebar-link'>
            <i data-feather="layers" width="20"></i>
            <span>Admin Menu</span>
        </a>
        <ul class="submenu ">
            <li>
                <a href="/tambah-berita">Tambah Berita</a>
            </li>
            <li>
                <a href="/">Tambah Penerbit</a>
            </li>
            <li>
                <a href="/">Kelola Berita</a>
            </li>
            <li>
                <a href="/">Kelola Genre</a>
            </li>
        </ul>
    </li>
    @elseif(Auth::user()->role === "Penerbit")
    <li class="sidebar-item  has-sub">
        <a href="#" class='sidebar-link'>
            <i data-feather="layers" width="20"></i>
            <span>Penerbit Menu</span>
        </a>
        <ul class="submenu ">
            <li>
                <a href="/">Tambah Berita</a>
            </li>
            <li>
                <a href="/">Tambah Penerbit</a>
            </li>
            <li>
                <a href="/">Kelola Berita</a>
            </li>
            <li>
                <a href="/">Kelola Genre</a>
            </li>
        </ul>
    </li>
    @endif
    <li class="sidebar-item">
        <a href="/profile" class='sidebar-link'>
            <i data-feather="user" width="20"></i>
            <span>Profile</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a href="/logout" class='sidebar-link'>
            <i data-feather="log-out" width="20"></i>
            <span>Logout</span>
        </a>
    </li>
</ul>