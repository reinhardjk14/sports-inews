<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Amin Template">
    <meta name="keywords" content="Amin, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Amin | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('amin-master/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/barfiller.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Menu Begin -->
    <div class="humberger-menu-overlay"></div>
    <div class="humberger-menu-wrapper">
        <div class="hw-logo"> // <--
            <a href="#"><img src="" alt=""></a>
        </div>
        <div class="hw-menu mobile-menu">
            <ul>
                <li><a href="./index.html">HOME</a></li>
                <li><a href="#">Admin <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown">
                        <li><a href="./categories-list.html">Dashboard</a></li>
                        <li><a href="./categories-list.html">My Account</a></li>
                        <li><a href="./categories-list.html">Sign Out</a></
                    </ul>
                </li>
            </ul>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div class="hw-copyright">
            Copyright © 2021 SportiNews Inc. All rights reserved
        </div>
        <div class="hw-social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-youtube-play"></i></a>
            <a href="#"><i class="fa fa-instagram"></i></a>
        </div>
        <div class="hw-insta-media">
            <div class="section-title">
                <h5>Instagram</h5>
            </div>
            <div class="insta-pic">
                <img src="{{asset('img/marquez.png')}}" alt="">
                <img src="{{asset('img/quartararo.png')}}" alt="">
                <img src="{{asset('img/rossi.png')}}" alt="">
                <img src="{{asset('img/bagnaia.png')}}" alt="">
            </div>
        </div>
    </div>
    <!-- Humberger Menu End -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="ht-options">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-8">
                        <div class="ht-widget">
                            <ul>
                                <li><i class="fa fa-clock-o"></i> {{date("M d, Y")}}</li>
                                @auth
                                <li><i class="fa fa-home"></i> <a href="/login" style="color: white;">Dashboard</a>
                                @endauth
                                @guest
                                <li><i class="fa fa-sign-in"></i> <a href="/login" style="color: white;">Sign In</a>
                                <li><i class="fa fa-sign-out"></i> <a href="/register" style="color: white;">Sign Up</a>
                                @endguest
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4">
                        <div class="ht-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="logo">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                       <a href="./index.html"><img src="{{asset('amin-master/img/logo.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-options">
            <div class="container">
                <div class="humberger-menu humberger-open">
                    <i class="fa fa-bars"></i>
                </div>
                <div class="nav-search search-switch">
                    <i class="fa fa-search"></i>
                </div>
                <div class="nav-menu">
                    <ul>
                        <li class="active"><a href="./index.html"><span>HOME</span></a></li>
                        <li class="mega-menu"><a href="#"><span>SEPAKBOLA <i class="fa fa-angle-down"></i></span></a>
                            <div class="megamenu-wrapper">
                                <ul class="mw-nav">
                                    <li><a href="#"><span>Liga Indonesia</span></a></li>
                                    <li><a href="#"><span>Liga Spanyol</span></a></li>
                                    <li><a href="#"><span>Liga Champions</span></a></li>
                                    <li><a href="#"><span>Liga Inggris</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#"><span>SPORTSTAINMENT <i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown">
                                <li><a href="#">SELEBSPORT</a></li>
                                <li><a href="#">LIFESTYLE</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>OTOMOTIF <i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown">
                                <li><a href="#">MOTOGP</a></li>
                                <li><a href="#">FORMULA 1</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>MULTISPORT <i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown">
                                <li><a href="#">BASKET</a></li>
                                <li><a href="#">FUTSAL 1</a></li>
                                <li><a href="#">BADMINTON</a></li>
                                <li><a href="#">MMA</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>E-SPORTS <i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown">
                                <li><a href="#">MOBILE LEGEND</a></li>
                                <li><a href="#">PUBG</a></li>
                                <li><a href="#">DOTA 2</a></li>
                                <li><a href="#">CSGO</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>SCHEDULES <i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown">
                                <li><a href="#">SCHEDULES</a></li>
                                <li><a href="#">STANDINGS</a></li>
                                <li><a href="#">LIVE SCORE</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>OTHERS <i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown">
                                <li><a href="#">GALLERY</a></li>
                                <li><a href="#">TIPS AND TRICKS</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>CONTACT US <i class="fa fa-angle-down"></i></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="hs-text">
                        <div class="label"><span>BREAKING!</span></div>
                        <h3>CR7 BACK TO THE REDS!</h3>
                        <div class="ht-meta">
                            <img src="{{asset('img/anon.png')}}" alt="">
                            <ul>
                                <li>by Jordi Kurniawan</li>
                                <li>Aug 25, 2021</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 offset-lg-1 offset-xl-2">
                    <div class="trending-post">
                        <div class="section-title">
                            <h5>Trending News</h5>
                        </div>
                        <div class="trending-slider owl-carousel">
                            <div class="single-trending-item">
                                <div class="trending-item">
                                   <div class="ti-pic"></div>
                                    <div class="ti-text">
                                        <h6><a href="/berita">Link Live Streaming Moto GP Aragon 2021 Saksikan di Sini!</a>
                                        </h6>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Sep 10, 2021</li>
                                            <li><i class="fa fa-comment-o"></i> 1</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="trending-item">
                                    <div class="ti-pic"></div>
                                    <div class="ti-text">
                                        <h6><a href="/berita">Link Live Streaming Trans 7 MotoGP</a></h6>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                            <li><i class="fa fa-comment-o"></i> 12</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="trending-item">
                                   <div class="ti-pic"></div>
                                    <div class="ti-text">
                                        <h6><a href="/berita">Berburu Podium di Aragon</a>
                                        </h6>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                            <li><i class="fa fa-comment-o"></i> 6</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="trending-item">
                                   <div class="ti-pic"></div>
                                    <div class="ti-text">
                                        <h6><a href="/berita">Christiano Ronaldo Menghukum Newcastle United</a>
                                        </h6>
                                        <ul>
                                            <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                            <li><i class="fa fa-comment-o"></i> 12</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-slider owl-carousel">
            <div class="hs-item set-bg" data-setbg="{{asset('img/ronaldo.jpg')}}"></div>
            <div class="hs-item set-bg" data-setbg="{{asset('img/ronaldo2.jpg')}}"></div>
            <div class="hs-item set-bg" data-setbg="{{asset('img/ronaldo3.jpg')}}"></div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Update News Section Begin -->
    <section class="update-news-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="section-title">
                        <h5><span>News & update</span></h5>
                    </div>
                    <div class="tab-elem">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="tabs-1" role="tabpanel">
                                <div class="row">
                                    <div class="un-slider owl-carousel">
                                        <div class="col-lg-12">
                                            <div class="un-big-item set-bg" data-setbg="{{asset('img/bola2.jpg')}}">
                                                <div class="ub-text">
                                                    <h5><a href="#">Christiano Ronaldo Menghukum Newcastle United </a></h5>
                                                    <ul>
                                                        <li>by <span>Admin</span></li>
                                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                        <li><i class="fa fa-comment-o"></i> 3</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Berburu Podium di Aragon</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 6</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp2.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Link Live Streaming Trans 7 MotoGP</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 11, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 0</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp3.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Link Live Streaming Moto GP Aragon 2021 Saksikan di Sini!</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 10, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 1</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="un-big-item set-bg" data-setbg="{{asset('img/motogp.jpg')}}">
                                                <div class="ub-text">
                                                    <h5><a href="#">Berburu Podium di Aragon</a></h5>
                                                    <ul>
                                                        <li>by <span>Admin</span></li>
                                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                        <li><i class="fa fa-comment-o"></i> 6</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp2.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Link Live Streaming Trans 7 MotoGP</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 11, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 0</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/bola2.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Christiano Ronaldo Menghukum Newcastle United</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 3</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs-2" role="tabpanel">
                                <div class="row">
                                    <div class="un-slider owl-carousel">
                                        <div class="col-lg-12">
                                            <div class="un-big-item set-bg" data-setbg="{{asset('img/bola2.jpg')}}">
                                                <div class="ub-text">
                                                    <h5><a href="#">Christiano Ronaldo Menghukum Newcastle United</a></h5>
                                                    <ul>
                                                        <li>by <span>Admin</span></li>
                                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                        <li><i class="fa fa-comment-o"></i> 3</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp2.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Link Live Streaming Trans 7 MotoGP</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 11, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 0</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="img/motogp.jpg">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Berburu Podium di Aragon</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 6</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp3.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Link Live Streaming Moto GP Aragon 2021 Saksikan di Sini!</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 10, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 1</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="un-big-item set-bg" data-setbg="img/motogp.jpg">
                                                <div class="ub-text">
                                                    <h5><a href="#">Berburu Podium di Aragon</a></h5>
                                                    <ul>
                                                        <li>by <span>Admin</span></li>
                                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                        <li><i class="fa fa-comment-o"></i> 6</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/bola2.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Christiano Ronaldo Menghukum Newcastle United</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 3</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp3.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Link Live Streaming Moto GP Aragon 2021 Saksikan di Sini!</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 10, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 1</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="un-item">
                                                        <div class="un_pic set-bg" data-setbg="{{asset('img/motogp.jpg')}}">
                                                        </div>
                                                        <div class="un_text">
                                                            <h6><a href="#">Berburu Podium di Aragon</a></h6>
                                                            <ul>
                                                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                                                <li><i class="fa fa-comment-o"></i> 6</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-option">
                        <div class="social-media">
                            <div class="section-title">
                                <h5>Social media</h5>
                            </div>
                            <ul>
                                <li>
                                    <div class="sm-icon"><i class="fa fa-facebook"></i></div>
                                    <span>Facebook</span>
                                    <div class="follow">18,2k Follow</div>
                                </li>
                                <li>
                                    <div class="sm-icon"><i class="fa fa-twitter"></i></div>
                                    <span>Twitter</span>
                                    <div class="follow">24,7k Follow</div>
                                </li>
                                <li>
                                    <div class="sm-icon"><i class="fa fa-youtube-play"></i></div>
                                    <span>Youtube</span>
                                    <div class="follow">2,3k Subs</div>
                                </li>
                                <li>
                                    <div class="sm-icon"><i class="fa fa-instagram"></i></div>
                                    <span>Instagram</span>
                                    <div class="follow">2,6k Follow</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Update News Section End -->



    <!-- Instagram Post Section Begin -->
    <section class="instagram-post-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="section-title">
                        <h5>Instagram</h5>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                            <img src="{{asset('img/bagnaia2.jpeg')}}" alt="">
                        </div>
                        <div class="ip-text">
                            <div class="label"><span>Hot</span></div>
                            <h5><a href="#">Gagal Kalahkan Francesco Bagnaia di MotoGP Aragon 2021, Marc Marquez...</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                <li><i class="fa fa-comment-o"></i> 3</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices...</p>
                        </div>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                            <img src="{{asset('img/oldtrafford.webp')}}" alt="">
                        </div>
                        <div class="ip-text">
                            <div class="label"><span>Hot</span></div>
                            <h5><a href="#">Old Trafford Menggila! Beginilah Reaksi Fans MU pada Comeback CR7...</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                <li><i class="fa fa-comment-o"></i> 5</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices...</p>
                        </div>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                            <img src="{{asset('img/jadipenyebab.jpg')}}" alt="">
                        </div>
                        <div class="ip-text">
                            <div class="label"><span>Hot</span></div>
                            <h5><a href="#">Jadi Penyebab Kecelakaan Hamilton di Formula 1 GP Italia, Verstappen...</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                <li><i class="fa fa-comment-o"></i> 4</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices...</p>
                        </div>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                            <img src="{{asset('img/kungfu.jpg')}}" alt="">
                        </div>
                        <div class="ip-text">
                            <div class="label"><span>Hot</span></div>
                            <h5><a href="#">Tanggapi Insiden Sepak Bola Kungfu, Menpora...</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                <li><i class="fa fa-comment-o"></i> 0</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-7">
                    <div class="sidebar-option">
                        <div class="insta-media">
                            <div class="section-title">
                                <h5>Instagram</h5>
                            </div>
                            <div class="insta-pic">
                                <img src="{{asset('img/marquez.png')}}" alt="">
                                <img src="{{asset('img/quartararo.png')}}" alt="">
                                <img src="{{asset('img/rossi.png')}}" alt="">
                                <img src="{{asset('img/bagnaia.png')}}" alt="">
                            </div>
                        </div>
                        <div class="best-of-post">
                            <div class="section-title">
                                <h5>TOP SEARCH</h5>
                            </div>
                            <div class="bp-item">
                                <div class="bp-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress-1" data-cpid="id-1" data-cpvalue="95"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">9.9</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bp-text">
                                    <h6><a href="#">CHRISTIANO RONALDO</a></h6>
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bp-item">
                                <div class="bp-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress-1" data-cpid="id-2" data-cpvalue="85"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">9.0</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bp-text">
                                    <h6><a href="#">LIONEL MESSI</a></h6>
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bp-item">
                                <div class="bp-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress-1" data-cpid="id-3" data-cpvalue="80"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">8.5</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bp-text">
                                    <h6><a href="#">LEBRON JAMES</a></h6>
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bp-item">
                                <div class="bp-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress-1" data-cpid="id-4" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">8.0</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bp-text">
                                    <h6><a href="#">CONNOR MCGREGOR</a></h6>
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> Sep 13, 2021</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="subscribe-option">
                            <div class="section-title">
                                <h5>Subscribe to our newsletter</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor.</p>
                            <form action="#">
                                <input type="text" placeholder="Name">
                                <input type="text" placeholder="Email">
                                <button type="submit"><span>Subscribe</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Instagram Post Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="footer-about">
                        <div class="fa-logo">
                            <a href="#"><img src="{{asset('amin-master/img/f-logo.png')}}" alt=""></a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua lacus vel facilisis.</p>
                        <div class="fa-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="tags-cloud">
                        <div class="section-title">
                            <h5>Tags Cloud</h5>
                        </div>
                        <div class="tag-list">
                            <a href="#"><span>MotoGP</span></a>
                            <a href="#"><span>Valentino Rossi</span></a>
                            <a href="#"><span>Christiano Ronaldo</span></a>
                            <a href="#"><span>Lionel Messi</span></a>
                            <a href="#"><span>Lebron James</span></a>
                            <a href="#"><span>Andakara Prastawa</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="ca-text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved<i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ca-links">
                            <a href="#">About</a>
                            <a href="#">Subscribe</a>
                            <a href="#">Contact</a>
                            <a href="#">Support</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Sign Up Section Begin -->
    <div class="signup-section">
        <div class="signup-close"><i class="fa fa-close"></i></div>
        <div class="signup-text">
            <div class="container">
                <div class="signup-title">
                    <h2>Sign up</h2>
                    <p>Fill out the form below to recieve a free and confidential</p>
                </div>
                <form action="#" class="signup-form">
                    <div class="sf-input-list">
                        <input type="text" class="input-value" placeholder="User Name*">
                        <input type="text" class="input-value" placeholder="Password">
                        <input type="text" class="input-value" placeholder="Confirm Password">
                        <input type="text" class="input-value" placeholder="Email Address">
                        <input type="text" class="input-value" placeholder="Full Name">
                    </div>
                    <div class="radio-check">
                        <label for="rc-agree">I agree with the term & conditions
                            <input type="checkbox" id="rc-agree">
                            <span class="checkbox"></span>
                        </label>
                    </div>
                    <button type="submit"><span>REGISTER NOW</span></button>
                </form>
            </div>
        </div>
    </div>
    <!-- Sign Up Section End -->

    <!-- Search model Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="{{asset('amin-master/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('amin-master/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('amin-master/js/circle-progress.min.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.barfiller.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('amin-master/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('amin-master/js/main.js')}}"></script>
</body>
</html>