@extends('layout.master-admin')
@section('judul')
iNews Sport - Profile
@endsection
@section('deskripsi')
Selamat datang di iNews Sport Profile
@endsection
@section('isi')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Profile Config</h4>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" id="username"
                            placeholder="{{Auth::user()->username}}" disabled>
                    </div>
    
                    <div class="form-group">
                        <label for="email">Email</label>
                        <small class="text-muted">eg.<i>someone@example.com</i></small>
                        <input type="text" class="form-control" name="email" id="email" value="{{Auth::user()->email}}">
                    </div>
    
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="{{Auth::user()->password}}">
                        </p>
                    </div>
    
                    <div class="form-file">
                        <label for="foto">Foto</label><br>
                        <input type="file" class="form-file-input" id="foto">
                    </div>
                    <br>
                    <div class="form-file">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
