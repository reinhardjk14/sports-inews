<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Amin Template">
    <meta name="keywords" content="Amin, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sport iNews | Sign Up</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('amin-master/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/barfiller.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/style.css')}}" type="text/css">
</head>
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header End -->
    <div class="signup-section" style="display: block;">
        <a href="/"><div class="signup-close"><i class="fa fa-close"></i></div></a>
        <div class="signup-text">
            <div class="container">
                <div class="signup-title">
                    <h2>Sign up</h2>
                    <p>Fill out the form below to recieve a free member and confidential</p>
                </div>
                <form action="/kirimRegister" method="POST" class="signup-form">
                    @csrf
                    <div class="sf-input-list">
                        <input type="text" class="input-value" name="username" placeholder="Username" required>
                        <input type="password" class="input-value" name="password" placeholder="Password" required>
                        <input type="email" class="input-value" name="email" placeholder="Email">
                    </div>
                    <div class="radio-check">
                        <label for="rc-agree">I agree with the <a href=#>term &amp; conditions</a>
                            <input type="checkbox" id="rc-agree">
                            <span class="checkbox"></span>
                        </label>
                    </div>
                    <button type="submit"><span>REGISTER NOW</span></button>
                </form>
            </div>
        </div>
    </div>
    <!-- Js Plugins -->
    <script src="{{asset('amin-master/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('amin-master/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('amin-master/js/circle-progress.min.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.barfiller.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('amin-master/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('amin-master/js/main.js')}}"></script>
</body>
</html>
