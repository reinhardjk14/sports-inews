@extends('layout.master-admin')
@section('judul')
iNews Sport - Dashboard
@endsection
@section('deskripsi')
Selamat datang di iNews Sport
@endsection
@section('isi')
<div class="row mb-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>Welcome</h4>
            </div>
            <div class="card-body">
                <h6>Selamat datang <b>{{ Auth::user()->username }}</b>  , kamu adalah <b>{{ Auth::user()->role }}</b></h6>
            </div>
        </div>
    </div>
</div>
@endsection
