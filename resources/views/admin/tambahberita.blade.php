@extends('layout.master-admin')
@section('judul')
iNews Sport - Tambah Berita
@endsection
@section('deskripsi')
Selamat datang di iNews Sport Berita
@endsection
@section('isi')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Tambah Berita</h4>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <form action="/addberita" method="POST" class="signup-form">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul Berita</label>
                        <input type="text" name="judul" class="form-control" id="judul">
                    </div>
    
                    <div class="form-group">
                        <label for="isi">Deskripsi Berita</label>
                        <textarea class="form-control" id="isi" name="isi" rows="3"></textarea>
                    </div>
    
                    <div class="form-group">
                        <label for="tanggalTerbit">Tanggal Terbit</label>
                        <input type="date" id="tanggalTerbit" name="tanggalTerbit" class="form-control" placeholder="{{Auth::user()->password}}">
                        </p>
                    </div>
    
                    <div class="form-file">
                        <label for="foto">Foto</label><br>
                        <input type="file" class="form-file-input" id="foto">
                    </div>
                    <br>
                    <div class="form-file">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
