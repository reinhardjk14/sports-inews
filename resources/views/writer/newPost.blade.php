<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Amin Template">
    <meta name="keywords" content="Amin, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sport iNews | Sign In</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('amin-master/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/barfiller.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('amin-master/css/style.css')}}" type="text/css">
</head>
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    
    <!-- Header End -->
    <div class="signup-section" style="display: block;">
        <div class="signup-close"><i class="fa fa-close"></i></div>
        <div class="signup-text">
            <div class="container">
                <div class="signup-title">
                    <h2>Create New Post</h2>
                    <p>Upload your post to our website</p>
                </div>
                <form action="/kirimLogin" method="POST" class="signup-form">
                    @csrf
                    <div class="sf-input-list">
                        <input type="text" class="input-value" name="title" placeholder="title">
                        <input type="text" class="input-value" name="content" placeholder="content">
                        <input type="date" class="input-value" name="publishedDate">
                        <input type="file" class="input-value" name="image">

                    </div>
                    <button type="submit"><span>LOGIN</span></button>
                </form>
            </div>
        </div>
    </div>
    <!-- Js Plugins -->
    <script src="{{asset('amin-master/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('amin-master/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('amin-master/js/circle-progress.min.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.barfiller.js')}}"></script>
    <script src="{{asset('amin-master/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('amin-master/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('amin-master/js/main.js')}}"></script>
</body>
</html>
