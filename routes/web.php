<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'AdminController@Dashboard')->name('dashboard');
    Route::get('/profile', 'AdminController@Profile')->name('profile');
    Route::get('/tambah-berita', 'AdminController@tambahberita');
    Route::post('/addberita', 'AdminController@addberita');
    Route::resource('/genre', 'KategoriController@x');
    Route::get('/logout', function ()
    {
        auth()->logout();
        Session()->flush();
    
        return Redirect::to('/');
    })->name('logout');
});

// HomeContoller
Route::get('/', 'HomeController@Home');
Route::get('/berita', 'HomeController@Berita');

// Sign In & Sign Up
Auth::routes();
